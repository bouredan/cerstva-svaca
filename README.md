# Čerstvá sváča  

### Projekt Čerstvá sváča je spojkou mezi rodiči a kvalitními, zdravými svačinami pro jejich děti.  
### Více informací o projektu naleznete na naší [WIKI](https://gitlab.fel.cvut.cz/bouredan/cerstva-svaca/wikis/home).