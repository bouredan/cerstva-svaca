---
theme: gaia
style: |
  section {
    font-family: JetBrains Mono;
  }
class:
  - invert
  - lead
---

# Čerstvá sváča
Daniel Bourek, Patrik Dvořáček, Klára Jakešová, Lukáš Majer, Vojtěch Štěpančík

---

# Pozorování problému

- Příprava svačin pro děti není jednoduchý úkol
- Dělat svačiny ráno → Vyhradit si čas
- Nakoupit svačiny dopředu → Nejsou čerstvé
- Dát dítěti peníze na svačinu → Bez kontroly

---

# Návrh řešení

Centralizovaný systém pro výrobu a objednávání svačin

---

![bg fit](./resources/business-analysis/ai-nakup-surovin.png)

---

![bg fit](./resources/business-analysis/tb-objednavani-svaciny.png)

---

# Identifikace požadavků

- Veřejně přístupný katalog svačin
- Jednoduchý přístup k objednávání

---

![bg fit](./resources/business-analysis/tb-model.png)

---

# Implementace požadavků

- Případy užití
- Hrubé návrhy obrazovek

---

# Životní cyklus uživatele

- _Rodič_ se zaregistruje
- Přidá do systému svoje děti (_Jedlíky_)
- Dobije si kredit
- Objednává

---

![bg fit](./resources/wireframe/zaregistrovat-se.png)

---

![bg fit](./resources/wireframe/pridat-jedlika.png)

---

![bg fit](./resources/wireframe/zobrazit-nabidku-pro-jedlika.png)

---

# Návrh nasazení

- Rozdělení systému na microservices
- Klientská komunikace přes GraphQL API gateway

---

![bg fit](./resources/sw-analysis/deployment-diagram.png)

---

# Závěrem

- Dobrá práce 🎉!

---

Otázky?


https://gitlab.fel.cvut.cz/bouredan/cerstva-svaca
